var cacheAdmin = require('./cacheAdmin.js');
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var basedir = null;
if ( process.argv[2] == '--basedir' && process.argv[3] ) {
    basedir = process.argv[3];
    process.chdir(basedir);
}

cacheAdmin.load( basedir );

var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json())

app.get('/caches', function(req, res){
    res.send( cacheAdmin.list() );
});

app.post('/caches', function(req, res){
    res.send( cacheAdmin.createCache(req.body) );
});

app.get('/caches/:nome', function(req, res){
    // Tratar qd não achar pra retornar um 404
    res.send( cacheAdmin.get( req.params.nome ) );
});

app.listen( 3000 );
console.log('A mágica acontece na porta 3000');
