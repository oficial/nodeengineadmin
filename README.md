# NodeEngineAdmin #

Aplicativo para administração de caches locais do aplicativo iEngine.exe.


### A quem se destina? ###

A desenvolvedores de software que utilizam o servidor de aplicação iEngine da empresa Bematech.

### Instalação ###

* Usando o git: $ git clone https://oficial@bitbucket.org/oficial/nodeengineadmin.git
* Configuration: ...

### Screenshot ###

![Captura_de_tela-Cache Admin - Mozilla Firefox-2.png](https://bitbucket.org/repo/poegzp/images/1763655440-Captura_de_tela-Cache%20Admin%20-%20Mozilla%20Firefox-2.png)