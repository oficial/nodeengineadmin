var glob = require('glob');
var path = require('path');
var http = require('http');
var ini  = require('ini');
var fs   = require('fs');
var spawn = require('child_process').spawn;

module.exports = {

    load : function cacheAdmin_load( basedir ){
        console.log('Carregando os caches a partir da pasta: '+ basedir );
        this.basedir = basedir;

        var bases = [];
        var clientes = [];

        this.bases = bases;
        this.clientes = clientes;

        glob( path.join(basedir,'**/base.cfg'), null,
        function on_glob_result( erro, arquivos ){
            var cacheConfig = null;
            for (var i = arquivos.length - 1; i >= 0; i--) {
                cacheConfig = ini.parse(fs.readFileSync( arquivos[i], 'utf-8'))
                if ( ! cacheConfig.base.pasta ) {
                    cacheConfig.base.pasta = path.dirname(arquivos[i]);
                }
                bases.push(cacheConfig.base);
                clientes.push(cacheConfig.base.cliente)
            };
        })
    },

    clients : function cacheAdmin_clients(){
        //
        return this.clients;
    },

    refresh : function cacheAdmin_refresh(){
        //
        this.load( this.basedir );
    },

    list : function cacheAdmin_list(){
        //
        return this.bases;
    },

    get : function cacheAdmin_get( nome ){
        for (var i = 0; i < this.bases.length; i++) {
            if ( this.bases[i].nome.toUpperCase() == nome.toUpperCase() ) {
                return this.bases[i];
            };
        };
        return null;
    },

    createCache : function cacheAdmin_create( novo ){
        console.log('Criando cache local da base '+ novo.nome );

        var pasta = this.createDir( novo );
        var config = this.createConfig( novo, pasta );
        this.downloadEngine( config );
        if ( novo.loadCache ) {
            this.startCache( config );
        }
        return {
            message:"OK"
        }
    },

    createDir : function cacheAdmin_createDir( novo ){

        if ( ! fs.existsSync( this.basedir )) {
            throw "Diretorio não encontrado: "+ this.basedir;
        }
        var dir_cliente = path.join(this.basedir, novo.cliente);
        var dir_base = path.join(dir_cliente, novo.nome);

        if ( ! fs.existsSync( dir_cliente )) {
            fs.mkdirSync( dir_cliente );
        }
        if ( ! fs.existsSync( dir_base )) {
            fs.mkdirSync( dir_base );
        }
        return dir_base;
    },

    createConfig : function cacheAdmin_createConfig( novo, pasta ){
        var arquivo_configuracao = path.join(pasta,'base.cfg');
        if ( fs.existsSync( arquivo_configuracao )) {
            throw "Arquivo de configuração já existe!";
        }
        fs.writeFileSync(
            arquivo_configuracao,
            ini.stringify({
                base : {
                    cliente :   novo.cliente,
                    nome :      novo.nome,
                    apelido:    novo.apelido,
                    url :       novo.url,
                    pasta:      pasta
                }
            })
        )
    },

    downloadEngine : function cacheAdmin_downloadEngine( config ){
        var iengine_exe = path.join(config.base.pasta, 'iEngine.exe');
        var file = fs.createWriteStream( iengine_exe );
        http.get( config.base.url +"/iengine.exe", function( response ) {
            response.pipe(file);
        });
    },

    startCache : function cacheAdmin_startCache( config ){
        var iengine_exe = path.join(config.base.pasta, 'iEngine.exe');
        if ( ! fs.existsSync( iengine_exe )) {
            throw 'Arquivo nao encontrado:'+ iengine_exe;
        }
        var out = fs.openSync(path.join(config.base.pasta,'out.log'), 'a');
        var err = fs.openSync(path.join(config.base.pasta,'out.log'), 'a');

        var child = spawn( iengine_exe, [ config.base.url, config.base.nome ], {
            detached: true,
            stdio: [ 'ignore', out, err ]
        });

        child.unref();
    }
}